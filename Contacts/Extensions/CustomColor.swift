//
//  CustomColor.swift
//  Contacts
//
//  Created by adarsh chaudhary on 20/07/19.
//  Copyright © 2019 adarsh. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    class func RGB(_ r:CGFloat, _ g:CGFloat, _ b:CGFloat) -> UIColor{
        return UIColor.init(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: 1.0)
    }
    
    class func RGBA(_ r:CGFloat, _ g:CGFloat, _ b:CGFloat, _ a:CGFloat)  -> UIColor{
        return UIColor.init(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: a)
    }
}

let kWhiteColor = UIColor.white
let kClearColor = UIColor.clear
let kRGBA80_227_194_28 = UIColor.RGBA(80.0, 227.0, 194.0, 0.28)
let kRGB80_227_194 = UIColor.RGB(80.0, 227.0, 194.0)
let kRGB254_56_36 = UIColor.RGB(245.0, 56.0, 36.0)
