//
//  CustomView.swift
//  Contacts
//
//  Created by adarsh chaudhary on 20/07/19.
//  Copyright © 2019 adarsh. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func addGradient(startColor:UIColor?, endColor:UIColor?, startPoint:CGPoint, endPoint:CGPoint) {
        let gradient = CAGradientLayer()
        gradient.frame = frame
        gradient.startPoint =  startPoint
        gradient.endPoint = endPoint
        gradient.colors = [startColor?.cgColor ?? kClearColor.cgColor, endColor?.cgColor ?? kClearColor.cgColor]
        self.layer.insertSublayer(gradient, at: 0)
    }
}
