//
//  ApiError.swift
//  Contacts
//
//  Created by adarsh chaudhary on 21/07/19.
//  Copyright © 2019 adarsh. All rights reserved.
//

import Foundation

class ApiError {
    var statusCode : String?
    var errorMessage : String?
    
    init(dict:[String:AnyObject]) {
        if let _ = dict["status"] {
            statusCode = dict["status"] as? String
        }
        if let _ = dict["error"] {
            errorMessage = dict["error"] as? String
        }
    }
}
