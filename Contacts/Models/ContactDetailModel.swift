//
//  ContactDetailModel.swift
//  Contacts
//
//  Created by adarsh chaudhary on 21/07/19.
//  Copyright © 2019 adarsh. All rights reserved.
//

import Foundation
import UIKit

class ContactDetailModel {
    var contactId : NSNumber?
    var firstName : String?
    var lastName : String?
    var fullName : String?
    var email : String?
    var phoneNumber : String?
    var profilePicUrl : String?
    var profilePic : UIImage?
    var isFavourite : Bool
    var createdAtDate : Date?
    var updatedAtDate : Date?
    var firstLetterOfName : String?
    
    init(dict : [String:AnyObject]) {
        if let _ = dict["id"] {
            contactId = dict["id"] as? NSNumber
        } else {
            contactId = NSNumber(value:-1)
        }
        if let _ = dict["first_name"] {
            firstName = dict["first_name"] as? String
            if (firstName?.first?.isLetter ?? false) {
                firstLetterOfName = "\(firstName?.first?.uppercased() ?? "#")"
            } else {
                firstLetterOfName = "#"
            }
        } else {
            firstLetterOfName = ""
            firstName = ""
        }
        if let _ = dict["last_name"] {
            lastName = dict["last_name"] as? String
        } else {
            lastName = ""
        }
        fullName = "\(firstName ?? "") \(lastName ?? "")"
        if let _ = dict["email"] {
            email = dict["email"] as? String
        } else {
            email = ""
        }
        if let _ = dict["phone_number"] {
            phoneNumber = dict["phone_number"] as? String
        } else {
            phoneNumber = ""
        }
        if let _ = dict["profile_pic"] {
            profilePicUrl = dict["profile_pic"] as? String
        } else {
            profilePicUrl = ""
        }
        if let _ = dict["favorite"] {
            isFavourite = dict["favorite"]!.boolValue
        } else {
            isFavourite = false
        }
        if let _ = dict["created_at"] {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            createdAtDate = dateFormatter.date(from: dict["created_at"] as! String)
        }
        if let _ = dict["updated_at"] {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            updatedAtDate = dateFormatter.date(from: dict["updated_at"] as! String)
        }
    }
}
