//
//  File.swift
//  Contacts
//
//  Created by adarsh chaudhary on 20/07/19.
//  Copyright © 2019 adarsh. All rights reserved.
//

import Foundation
import UIKit

class Contacts {
    
    var profilePic : UIImage?
    let profilePicUrl : String?
    let firstName : String?
    let lastName : String?
    let fullName : String?
    let contactId : NSNumber?
    let isFavorite : Bool
    let detailUrl : String?
    var firstLetterOfName : String?
    
    init(json:[String:AnyObject]) {
        if let _ = json["id"] {
            contactId = json["id"] as? NSNumber
        } else {
            contactId = NSNumber(value:-1)
        }
        if let _ = json["first_name"] {
            firstName = json["first_name"] as? String
            if (firstName?.first?.isLetter ?? false) {
                firstLetterOfName = "\(firstName?.first?.uppercased() ?? "#")"
            } else {
                firstLetterOfName = "#"
            }
        } else {
            firstLetterOfName = ""
            firstName = ""
        }
        if let _ = json["last_name"] {
            lastName = json["last_name"] as? String
        } else {
            lastName = ""
        }
        fullName = "\(firstName ?? "") \(lastName ?? "")"
        if let _ = json["profile_pic"] {
            profilePicUrl = json["profile_pic"] as? String
        } else {
            profilePicUrl = ""
        }
        if let _ = json["favorite"] {
            isFavorite = json["favorite"]!.boolValue
        } else {
            isFavorite = false
        }
        if let _ = json["url"] {
            detailUrl = json["url"] as? String
        } else {
            detailUrl = ""
        }
    }
}
