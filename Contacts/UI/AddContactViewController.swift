//
//  AddContactViewController.swift
//  Contacts
//
//  Created by adarsh chaudhary on 21/07/19.
//  Copyright © 2019 adarsh. All rights reserved.
//

import UIKit
protocol ProtocolAddContactViewController : class {
    func updateListWithEditedContact(contact: ContactDetailModel, indexPath:IndexPath?)
}

class AddContactViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    weak var delegate : ProtocolAddContactViewController?
    var contactModel : ContactDetailModel?
    var indexPath : IndexPath?
    let viewModel = AddContactViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.delegate = self
        registerNibs()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavBar()
    }
    
    func registerNibs() {
        tableView.register(UINib.init(nibName: "TextFieldCell", bundle: nil), forCellReuseIdentifier: "textFieldCell")
    }
    
    func setupView() {
        activityIndicator.stopAnimating()
        tableView.accessibilityIdentifier = "addContactTable"
        let headerView = DetailHeaderView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 240))
        headerView.headerDelegate = self
        headerView.isEditView = true
        headerView.updateView(name:contactModel?.fullName ?? "", profilePic: contactModel?.profilePicUrl ?? "", isFavourite:false)
        self.tableView.tableHeaderView = headerView
        tableView.tableFooterView = UIView()
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: nil) { (notification) in
            self.keyboardWillShow(notification:notification)
        }
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification, object: nil, queue: nil) { (notification) in
            self.keyboardWillHide(notification:notification)
        }
    }
    
    func keyboardWillShow(notification: Notification)  {
        guard let userInfo = notification.userInfo, let frame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        let bottomHeight = (self.view.frame.size.height-frame.height)/4 + frame.height
        let contentInset = UIEdgeInsets(top: 0, left: 0, bottom: bottomHeight, right: 0)
        tableView.contentInset = contentInset
    }
    
    func keyboardWillHide(notification: Notification)  {
        tableView.contentInset = UIEdgeInsets.zero
    }

    func setupNavBar() {
        let leftBarItem = UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(cancelBtnClicked))
        leftBarItem.tintColor =  kRGB80_227_194
        let rightBarItem = UIBarButtonItem.init(title: "Done", style: .plain, target: self, action: #selector(doneBtnClicked))
        rightBarItem.tintColor =  kRGB80_227_194
        navigationItem.leftBarButtonItem = leftBarItem
        navigationItem.rightBarButtonItem = rightBarItem
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
    }
    
    @objc func cancelBtnClicked() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func doneBtnClicked() {
        activityIndicator.startAnimating()
        var params : [String:AnyObject] = ["favorite" : (contactModel?.isFavourite ?? false) as AnyObject]
        for i in 0...4 {
            let cell = tableView.cellForRow(at: IndexPath.init(row: i, section: 0)) as? TextFieldCell
            switch i {
            case 0 :
                params["first_name"] = cell?.txtField.text as AnyObject?
            case 1 :
                params["last_name"] = cell?.txtField.text as AnyObject?
            case 2 :
                params["phone_number"] = cell?.txtField.text as AnyObject?
            case 3 :
                params["email"] = cell?.txtField.text as AnyObject?
            default:
                print("This condition will never come")
            }
        }
        if let _ = contactModel {
            viewModel.editContact(params:params, contactId:contactModel?.contactId)
        } else {
            viewModel.addContact(params: params)
        }
    }
}

extension AddContactViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : TextFieldCell = tableView.dequeueReusableCell(withIdentifier: "textFieldCell") as! TextFieldCell
        switch indexPath.row {
        case 0:
            cell.updateCell(lblText: "First Name", txtFieldText: contactModel?.firstName, isTxtFieldEditable: true)
        case 1:
            cell.updateCell(lblText: "Last Name", txtFieldText: contactModel?.lastName, isTxtFieldEditable: true)
        case 2:
            cell.updateCell(lblText: "Mobile", txtFieldText: contactModel?.phoneNumber, isTxtFieldEditable: true)
        case 3:
            cell.updateCell(lblText: "Email", txtFieldText: contactModel?.email, isTxtFieldEditable: true)
        default:
            print("This conditions will never come")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56
    }
}

extension AddContactViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var pickedImage = info[.editedImage]
        if pickedImage == nil{
            pickedImage = info[.originalImage]
        }
        let headerView = tableView.tableHeaderView as? DetailHeaderView
        headerView?.profileImgView.image = pickedImage as? UIImage
            picker.dismiss(animated: true, completion: nil)
    }
}

extension AddContactViewController : ProtocolDetailHeaderView {
    func cameraBtnClicked() {
        let actionSheet = UIAlertController.init(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        actionSheet.addAction(UIAlertAction.init(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { (action) in
            self.dismiss(animated: true, completion: {})
        }))
        let imagePicker = UIImagePickerController.init()
        imagePicker.delegate = self
        actionSheet.addAction(UIAlertAction.init(title: "Open Library", style: UIAlertAction.Style.default, handler: { (action) in
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }))
        actionSheet.addAction(UIAlertAction.init(title: "Open Camera", style: UIAlertAction.Style.default, handler: { (action) in
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            self.present(imagePicker, animated: true, completion: nil)
        }))
        self.present(actionSheet, animated: true, completion: nil)
    }
}

extension AddContactViewController : ProtocolAddContactViewModel {
    func contactAdded(error: ApiError?, response:ContactDetailModel?) {
        DispatchQueue.main.async {
            if let _ = response {
                self.activityIndicator.stopAnimating()
                var userInfo : [String:AnyObject] = [:]
                if let _ = response {
                    userInfo["contactDetail"] = response
                }
                if let _ = self.indexPath {
                    userInfo["isEdited"] = true as AnyObject
                } else {
                    userInfo["isEdited"] = false as AnyObject
                }
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ContactDetailsUpdated"), object: nil, userInfo: userInfo)
                self.delegate?.updateListWithEditedContact(contact: response!, indexPath: self.indexPath)
                self.dismiss(animated: true, completion: nil)
            } else {
                self.activityIndicator.stopAnimating()
                let alert = UIAlertController.init(title: "Error", message: error?.errorMessage ?? "Oops!! Something went wrong", preferredStyle: .alert)
                alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: { (action) in
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
}
