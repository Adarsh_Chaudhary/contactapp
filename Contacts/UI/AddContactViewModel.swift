//
//  AddContactViewModel.swift
//  Contacts
//
//  Created by adarsh chaudhary on 21/07/19.
//  Copyright © 2019 adarsh. All rights reserved.
//

import Foundation

protocol ProtocolAddContactViewModel : class {
    func contactAdded(error:ApiError?, response:ContactDetailModel?)
}

class AddContactViewModel {
    weak var delegate : ProtocolAddContactViewModel?
    
    func addContact(params:[String:AnyObject]?) {
        RequestManager.sharedManager.addContact(params: params) { (response, error) in
            if let _ = error {
                self.delegate?.contactAdded(error: error, response:nil)
            } else {
                self.delegate?.contactAdded(error: nil, response:response)
            }
        }
    }

    func editContact(params:[String:AnyObject]?, contactId:NSNumber?) {
        if let _ = contactId {
            RequestManager.sharedManager.editContact(params: params, contactId:contactId!) { (response, error) in
                if let _ = error {
                    self.delegate?.contactAdded(error: error, response:nil)
                } else {
                    self.delegate?.contactAdded(error: nil, response:response)
                }
            }
        }
    }
}
