//
//  ContactButtonView.swift
//  Contacts
//
//  Created by adarsh chaudhary on 20/07/19.
//  Copyright © 2019 adarsh. All rights reserved.
//

import UIKit

protocol ProtocolContactButtonView : class {
    func buttonClicked(type:String?)
}

class ContactButtonView: UIView {

    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var label: UILabel!
    weak var delegate : ProtocolContactButtonView?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    func setupView() {
        let bundle =  Bundle(for: type(of: self))
        let nib = UINib(nibName: "ContactButtonView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = bounds
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        addSubview(view)
    }
    
    func updateView(text:String?, icon:UIImage?) {
        if let _ = text {
            label.text = text
        } else {
            label.text = ""
        }
        if let _ = icon {
            button.setImage(icon, for: .normal)
        }
    }
    
    @IBAction func btnClicked(_ sender: Any) {
        delegate?.buttonClicked(type: label.text)
    }
}
