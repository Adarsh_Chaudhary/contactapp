//
//  ContactDetailViewController.swift
//  Contacts
//
//  Created by adarsh chaudhary on 19/07/19.
//  Copyright © 2019 adarsh. All rights reserved.
//

import UIKit
import MessageUI

protocol ProtocolContactDetailViewController : class {
    func contactDeleted(indexPath:IndexPath, contactModel:ContactDetailModel)
}

class ContactDetailViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    weak var delegate : ProtocolContactDetailViewController?
    var detailLink : String = String()
    var viewModel = ContactDetailViewModel()
    var indexPath : IndexPath?
    var contactDetailModel : ContactDetailModel?
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var cellArray : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.delegate = self
        tableView.contentInset = UIEdgeInsets(top: -44, left: 0, bottom: 0, right: 0)
        registerNibs()
        fetchData()
    }
    
    func fetchData() {
        activityIndicator.startAnimating()
        viewModel.fetchContactDetails(url: detailLink, params: nil)
    }
    
    func updateCellArray() {
        cellArray.removeAll()
        if contactDetailModel?.phoneNumber != nil && contactDetailModel?.phoneNumber != "" {
            cellArray.append("phone")
        }
        if contactDetailModel?.email != nil && contactDetailModel?.email != "" {
            cellArray.append("email")
        }
    }
    
    func registerNibs() {
        tableView.register(UINib.init(nibName: "TextFieldCell", bundle: nil), forCellReuseIdentifier: "textFieldCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavBar()
    }
    
    func setupNavBar() {
        let rightBarItem = UIBarButtonItem.init(title: "Edit", style: .plain, target: self, action: #selector(editContactsClicked))
        navigationItem.rightBarButtonItem = rightBarItem
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
    }
    
    @objc func editContactsClicked() {
        let controller = AddContactViewController.init(nibName: "AddContactViewController", bundle: nil)
        controller.contactModel = contactDetailModel
        controller.indexPath = indexPath
        controller.delegate = self
        let navigationController = UINavigationController.init(rootViewController: controller)
        present(navigationController, animated: true, completion: nil)
    }
}

extension ContactDetailViewController : ProtocolAddContactViewController {
    func updateListWithEditedContact(contact: ContactDetailModel, indexPath: IndexPath?) {
        var userInfo : [String:AnyObject] = [:]
        userInfo["contactDetail"] = contact
        userInfo["isEdited"] = true as AnyObject
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ContactDetailsUpdated"), object: nil, userInfo: userInfo)
        updateContactDetails(contactDetail: contact)
    }
}

extension ContactDetailViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return cellArray.count
        }
        if let _ = contactDetailModel {
            return 1
        }
        return 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell : TextFieldCell = tableView.dequeueReusableCell(withIdentifier: "textFieldCell") as! TextFieldCell
            if cellArray[indexPath.row] == "phone" {
                cell.updateCell(lblText: "Mobile", txtFieldText: contactDetailModel?.phoneNumber, isTxtFieldEditable: false)
            } else if cellArray[indexPath.row] == "email" {
                cell.updateCell(lblText: "Email", txtFieldText: contactDetailModel?.email, isTxtFieldEditable: false)
            }
            return cell
        }
        var cell : UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "deleteCell")
        if cell == nil {
            cell = UITableViewCell.init(style: .default, reuseIdentifier: "deleteCell")
        }
        cell?.textLabel?.text = "Delete"
        cell?.backgroundColor = kWhiteColor
        cell?.textLabel?.textColor = kRGB254_56_36
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 20
        }
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 56
        }
        return 44
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            let alert = UIAlertController.init(title: "Delete Contact", message: "Are you sure you want to delete \(contactDetailModel?.fullName ?? "")", preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "Delete", style: .destructive, handler: { (action) in
                if let _ = self.contactDetailModel?.contactId {
                    self.activityIndicator.startAnimating()
                    self.viewModel.deleteContact(contactId:(self.contactDetailModel?.contactId)!)
                }
            }))
            alert.addAction(UIAlertAction.init(title: "Cancel", style: .default, handler: { (action) in
                
            }))
            present(alert, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 30
        } else {
            return CGFloat.leastNormalMagnitude
        }
    }
}

//MARK: To handle clicks for Buttons
extension ContactDetailViewController {
    func callClicked() {
        if contactDetailModel?.phoneNumber != nil && contactDetailModel?.phoneNumber != ""{
            UIApplication.shared.open(URL.init(string: "tel://\(contactDetailModel?.phoneNumber ?? "")")!, options: [:], completionHandler: nil)
        } else {
            let alert = UIAlertController.init(title: "Error", message: "Phone number is not added to call. Please edit the contact to add detail", preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: { (action) in
                self.editContactsClicked()
            }))
            alert.addAction(UIAlertAction.init(title: "Cancel", style: .default, handler: { (action) in
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func messageBtnClicked() {
        if contactDetailModel?.phoneNumber != nil && contactDetailModel?.phoneNumber != "" {
            let sms: String = "sms:\(contactDetailModel?.phoneNumber ?? "")"
            let strURL: String = sms.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            UIApplication.shared.open(URL.init(string: strURL)!, options: [:], completionHandler: nil)
        } else {
            let alert = UIAlertController.init(title: "Error", message: "Phone number is not added to call. Please edit the contact to add detail", preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: { (action) in
                self.editContactsClicked()
            }))
            alert.addAction(UIAlertAction.init(title: "Cancel", style: .default, handler: { (action) in
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func favoriteBtnClicked(){
        viewModel.editContact(params: ["favorite" : !(contactDetailModel?.isFavourite ?? true) as AnyObject], contactId: contactDetailModel?.contactId)
    }
    
    func emailBtnClicked() {
        if contactDetailModel?.email != nil && contactDetailModel?.email != "" {
            if MFMailComposeViewController.canSendMail(){
                let mail = MFMailComposeViewController.init()
                mail.mailComposeDelegate = self
                mail.setToRecipients([contactDetailModel?.email ?? ""])
                self.present(mail, animated: true, completion: nil)
            } else {
                var recipients = "mailto:\(contactDetailModel?.email ?? "")"
                recipients = recipients.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed) ?? ""
                UIApplication.shared.open(URL.init(string: recipients)!, options: [:]) { (success) in
                }
            }
        } else {
            let alert = UIAlertController.init(title: "Error", message: "Email is not added to call. Please edit the contact to add detail", preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: { (action) in
                self.editContactsClicked()
            }))
            alert.addAction(UIAlertAction.init(title: "Cancel", style: .default, handler: { (action) in
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension ContactDetailViewController : ProtocolContactButtonView {
    func buttonClicked(type: String?) {
        switch type {
        case "Call":
            callClicked()
        case "Message":
            messageBtnClicked()
        case "Favourite" :
            favoriteBtnClicked()
        case "Email" :
            emailBtnClicked()
        default :
            print("There are no cases left so this wont get executed")
        }
    }
}

extension ContactDetailViewController : ProtocolContactDetailViewModel {
    func updateViewForError(error: ApiError) {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
            self.tableView.tableHeaderView = CommonUtility.errorViewForTableHeader(text: error.errorMessage ?? "Oops!! Something went wrong", frame: CGRect.init(x: 0, y: 0, width: self.tableView.frame.size.width, height: 50))
            self.cellArray.removeAll()
            self.tableView.reloadData()
        }
    }
    
    func contactEdited(contactDetail: ContactDetailModel) {
        DispatchQueue.main.async {
            self.updateListWithEditedContact(contact: contactDetail, indexPath: nil)
        }
    }
    
    func updateContactDetails(contactDetail: ContactDetailModel) {
        contactDetailModel = contactDetail
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
            let headerView = DetailHeaderView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 320))
            headerView.parentController = self
            headerView.updateView(name:contactDetail.fullName ?? "", profilePic: contactDetail.profilePicUrl ?? "", isFavourite:contactDetail.isFavourite)
            self.tableView.tableHeaderView = headerView
            self.updateCellArray()
            self.tableView.reloadData()
        }
    }
    
    func contactDeleted(success:Bool, error:ApiError?) {
        DispatchQueue.main.async {
            if success {
                self.activityIndicator.stopAnimating()
                self.navigationController?.popViewController(animated: true)
                if let _ = self.indexPath {
                    self.delegate?.contactDeleted(indexPath: self.indexPath!, contactModel:self.contactDetailModel!)
                }
            } else {
                self.activityIndicator.stopAnimating()
                let alert = UIAlertController.init(title: "Error", message: error?.errorMessage ?? "Oops!! Something went wrong", preferredStyle: .alert)
                alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: { (action) in
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
}

extension ContactDetailViewController : MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        dismiss(animated: true, completion: nil)
    }
}
