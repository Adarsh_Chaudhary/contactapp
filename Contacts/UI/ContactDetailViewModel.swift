//
//  File.swift
//  Contacts
//
//  Created by adarsh chaudhary on 21/07/19.
//  Copyright © 2019 adarsh. All rights reserved.
//

import Foundation
import UIKit

protocol ProtocolContactDetailViewModel : class {
    func updateContactDetails(contactDetail : ContactDetailModel)
    func updateViewForError(error:ApiError)
    func contactDeleted(success:Bool, error:ApiError?)
    func contactEdited(contactDetail : ContactDetailModel)
}

class ContactDetailViewModel {
    weak var delegate : ProtocolContactDetailViewModel?
    
    func fetchContactDetails(url:String?, params:[String : AnyObject]?) {
        RequestManager.sharedManager.getContactDetail(urlString: url ?? "", params: nil) { (contactDetailModel, error) in
            if let _ = error {
                self.delegate?.updateViewForError(error: error!)
            } else {
                self.delegate?.updateContactDetails(contactDetail: contactDetailModel!)
            }
        }
    }
    
    func deleteContact(contactId:NSNumber) {
        RequestManager.sharedManager.deleteContactDetails(params: nil, contactId: contactId) { (response, error) in
            if let _ = error {
                self.delegate?.contactDeleted(success:false, error:error)
            } else {
                self.delegate?.contactDeleted(success:true, error:nil)
            }
        }
    }
    
    func editContact(params:[String:AnyObject]?, contactId:NSNumber?) {
        if let _ = contactId {
            RequestManager.sharedManager.editContact(params: params, contactId:contactId!) { (response, error) in
                if let _ = error {
                    self.delegate?.contactDeleted(success: false, error: error)
                } else {
                    self.delegate?.contactEdited(contactDetail: response!)
                }
            }
        }
    }
}
