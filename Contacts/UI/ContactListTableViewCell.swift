//
//  ContactListTableViewCell.swift
//  Contacts
//
//  Created by adarsh chaudhary on 19/07/19.
//  Copyright © 2019 adarsh. All rights reserved.
//

import UIKit

class ContactListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak var contactNameLbl: UILabel!
    @IBOutlet weak var favoriteImgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func updateCell(profileImg:UIImage?, name:String, isFavorite:Bool) {
        if let _ = profileImg {
            profileImgView.image = profileImg
        } else {
            profileImgView.image = UIImage.init(named: "photo_placeholder")
        }
        contactNameLbl.text = name
        if isFavorite {
            favoriteImgView.image = UIImage.init(named: "favorite_home")
        } else {
            favoriteImgView.image = UIImage()
        }
    }
}
