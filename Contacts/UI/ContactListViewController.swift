//
//  ContactListViewController.swift
//  Contacts
//
//  Created by adarsh chaudhary on 19/07/19.
//  Copyright © 2019 adarsh. All rights reserved.
//

import UIKit

class ContactListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var contactsArray : [Contacts] = []
    var sectionTitles : [String] = []
    var sortedContactArray : [String : [Contacts]] = [:]
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var contactListViewModel = ContactListViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerNibs()
        contactListViewModel.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(updateListForContactDetail(notification:)), name: NSNotification.Name(rawValue: "ContactDetailsUpdated"), object: nil)
        activityIndicator.accessibilityIdentifier = "contactListIndicator"
        tableView.accessibilityIdentifier = "contactList"
        fetchData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavBar()
    }
    
    func setupNavBar() {
        let leftBarItem = UIBarButtonItem.init(title: "Groups", style: .plain, target: self, action: #selector(groupBtnClicked))
        leftBarItem.accessibilityIdentifier = "groupsNavBtn"
        let rightBarItem = UIBarButtonItem.init(title: "+", style: .plain, target: self, action: #selector(addContactsClicked))
        rightBarItem.accessibilityIdentifier = "addContactNavBtn"
        rightBarItem.setTitleTextAttributes([NSAttributedString.Key.font : UIFont.systemFont(ofSize: 30)], for: .normal)
        navigationItem.leftBarButtonItem = leftBarItem
        navigationItem.rightBarButtonItem = rightBarItem
    }
    
    func registerNibs() {
        tableView.register(UINib.init(nibName: "ContactListTableViewCell", bundle: nil), forCellReuseIdentifier: "contactListCell")
    }
    
    func fetchData() {
        activityIndicator.startAnimating()
        contactListViewModel.fetchContacts()
    }
    
    func processContactList(contacts : [Contacts]?) {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
        }
        if let _ = contacts {
            contactsArray = contacts ?? []
            if contactsArray.count > 0 {
                getSectionTitles()
            } else {
                tableView.tableHeaderView = CommonUtility.errorViewForTableHeader(text: "No Contacts Found", frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 50))
            }
        }
    }
    
    func getSectionTitles() {
        for item in contactsArray{
            if (sortedContactArray.keys.sorted().contains(item.firstLetterOfName ?? "#")){
                var array = sortedContactArray[item.firstLetterOfName ?? "#"]
                array?.append(item)
                sortedContactArray[item.firstLetterOfName ?? "#"] = array
            }else{
                sortedContactArray[item.firstLetterOfName ?? "#"] = [item]
                sectionTitles.append(item.firstLetterOfName ?? "#")
            }
        }
        sectionTitles.sort()
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    @objc func addContactsClicked() {
        let controller = AddContactViewController.init(nibName: "AddContactViewController", bundle: nil)
        let navigationController = UINavigationController.init(rootViewController: controller)
        present(navigationController, animated: true, completion: nil)
    }
    
    @objc func groupBtnClicked() {
        
    }
}

extension ContactListViewController  {
    @objc func updateListForContactDetail(notification:NSNotification) {
        if let contact : ContactDetailModel = notification.userInfo?["contactDetail"] as? ContactDetailModel {
            var contactArray = sortedContactArray[contact.firstLetterOfName ?? "#"]
            let dict : [String:AnyObject] = [
                "id" : contact.contactId as AnyObject,
                "first_name": contact.firstName as AnyObject,
                "last_name": contact.lastName as AnyObject,
                "email": contact.email as AnyObject,
                "phone_number": contact.phoneNumber as AnyObject,
                "profile_pic": contact.profilePicUrl as AnyObject,
                "favorite": false as AnyObject,
            ]
            let contactModel = Contacts.init(json:dict)
            if (notification.userInfo?["isEdited"] as? Bool) ?? false {
                var index : Int = -1
                for (i, contact) in contactArray?.enumerated() ?? [].enumerated() {
                    if contact.contactId == contactModel.contactId {
                        index = i
                        break
                    }
                }
                contactArray?.remove(at: index)
                contactArray?.insert(contactModel, at: index)
                sortedContactArray[contactModel.firstLetterOfName ?? "#"] = contactArray
            } else {
                contactArray?.append(contactModel)
                sortedContactArray[contact.firstLetterOfName ?? "#"] = contactArray
            }
            if let _ = sectionTitles.firstIndex(of: contact.firstLetterOfName ?? "#") {
                tableView.beginUpdates()
                tableView.reloadSections(IndexSet.init(integer: sectionTitles.firstIndex(of: contact.firstLetterOfName ?? "#")!), with: .fade)
                tableView.endUpdates()
            }
        }
    }
}

extension ContactListViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return sortedContactArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let contactArray = sortedContactArray[sectionTitles[section]]
        return contactArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactListCell") as! ContactListTableViewCell
        let contactArray = sortedContactArray[sectionTitles[indexPath.section]]
        let contactModel = contactArray?[indexPath.row]
        if contactModel?.profilePic == nil{
            if contactModel?.profilePicUrl != "" {
                RequestManager.sharedManager.getImage(url: contactModel?.profilePicUrl) { (image) in
                    contactModel?.profilePic = image
                    DispatchQueue.main.async {
                        cell.profileImgView.image = image
                    }
                }
            }
        }
        cell.updateCell(profileImg: contactModel?.profilePic, name: contactModel?.fullName ?? "", isFavorite: contactModel?.isFavorite ?? false)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionTitles[section]
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return sectionTitles
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 28
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailController = ContactDetailViewController.init(nibName: "ContactDetailViewController", bundle: nil)
        let contactArray = sortedContactArray[sectionTitles[indexPath.section]]
        let contactModel = contactArray?[indexPath.row]
        detailController.indexPath = indexPath
        detailController.delegate = self
        detailController.detailLink = contactModel?.detailUrl ?? ""
        navigationController?.pushViewController(detailController, animated: true)
    }
}

extension ContactListViewController : ProtocolContactDetailViewController {
    func contactDeleted(indexPath: IndexPath, contactModel:ContactDetailModel) {
        var contactArray = sortedContactArray[contactModel.firstLetterOfName ?? "#"]
        var index : Int = -1
        for (i, contact) in contactArray?.enumerated() ?? [].enumerated() {
            if contact.contactId == contactModel.contactId {
                index = i
                break
            }
        }
        contactArray?.remove(at: index)
        sortedContactArray[contactModel.firstLetterOfName ?? "#"] = contactArray
        tableView.beginUpdates()
        tableView.deleteRows(at: [indexPath], with: .fade)
        tableView.endUpdates()
    }
}

extension ContactListViewController : ProtocolContactListViewModel {
    func updateTableView(contactList: [Contacts]) {
        processContactList(contacts: contactList)
    }
    
    func updateForError(error: ApiError) {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
            self.tableView.tableHeaderView = CommonUtility.errorViewForTableHeader(text: error.errorMessage ?? "Oops!! Something went wrong", frame: CGRect(x: 0, y: 0, width: self.tableView.frame.size.width, height: 50))
        }
    }
}
