//
//  File.swift
//  Contacts
//
//  Created by adarsh chaudhary on 21/07/19.
//  Copyright © 2019 adarsh. All rights reserved.
//

import Foundation

protocol ProtocolContactListViewModel : class {
    func updateTableView(contactList:[Contacts])
    func updateForError(error:ApiError)
}

class ContactListViewModel {
    weak var delegate : ProtocolContactListViewModel?
    
    func fetchContacts() {
        RequestManager.sharedManager.getContactList(params: nil) { (contacts, error) in
            if let _ = error {
                self.delegate?.updateForError(error: error!)
            } else {
                self.delegate?.updateTableView(contactList: contacts ?? [])
            }
        }
    }
}
