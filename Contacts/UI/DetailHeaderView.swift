//
//  DetailHeaderView.swift
//  Contacts
//
//  Created by adarsh chaudhary on 20/07/19.
//  Copyright © 2019 adarsh. All rights reserved.
//

import UIKit

protocol ProtocolDetailHeaderView : class {
    func cameraBtnClicked()
}

class DetailHeaderView: UIView {
    
    @IBOutlet weak var cameraBtn: UIButton!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var contactNameLbl: UILabel!
    @IBOutlet weak var profileImgView: UIImageView!
    var parentController : ContactDetailViewController?
    var isEditView : Bool = false
    weak var headerDelegate : ProtocolDetailHeaderView?
    @IBOutlet weak var msgButton: ContactButtonView!
    @IBOutlet weak var callBtn: ContactButtonView!
    @IBOutlet weak var emailBtn: ContactButtonView!
    @IBOutlet weak var favoriteBtn: ContactButtonView!
    @IBOutlet weak var stackViewHeightConstraint: NSLayoutConstraint!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    @IBAction func cameraBtnClicked(_ sender: Any) {
        headerDelegate?.cameraBtnClicked()
    }
    func setupView() {
        let bundle =  Bundle(for: type(of: self))
        let nib = UINib(nibName: "DetailHeaderView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = bounds
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        view.addGradient(startColor: kWhiteColor, endColor: kRGBA80_227_194_28, startPoint: CGPoint(x: 0, y: 0), endPoint: CGPoint(x: 0, y: 1))
        addSubview(view)
    }
    
    func updateView(name:String, profilePic:String?, isFavourite:Bool) {
        if isEditView {
            stackView.isHidden = true
            cameraBtn.isHidden = false
            contactNameLbl.text = ""
            stackViewHeightConstraint.constant = 0.0
            profileImgView.image = UIImage(named:"photo_placeholder")
        } else {
            cameraBtn.isHidden = true
            stackView.isHidden = false
            stackViewHeightConstraint.constant = 80.0
            contactNameLbl.text = name
            msgButton.updateView(text : "Message", icon : UIImage(named:"message"))
            msgButton.delegate = parentController
            callBtn.updateView(text: "Call", icon: UIImage(named:"call"))
            callBtn.delegate = parentController
            emailBtn.updateView(text: "Email", icon: UIImage(named:"email"))
            emailBtn.delegate = parentController
            favoriteBtn.updateView(text: "Favourite", icon: (isFavourite) ? UIImage(named:"favorite_selected") : UIImage(named:"favorite_unselected"))
            favoriteBtn.delegate = parentController
            RequestManager.sharedManager.getImage(url: profilePic) { (image) in
                DispatchQueue.main.async {
                    self.profileImgView.image = image
                }
            }
        }
    }
}
