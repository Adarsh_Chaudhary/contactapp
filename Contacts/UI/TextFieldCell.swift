//
//  TextFieldCell.swift
//  Contacts
//
//  Created by adarsh chaudhary on 21/07/19.
//  Copyright © 2019 adarsh. All rights reserved.
//

import UIKit

class TextFieldCell: UITableViewCell {

    @IBOutlet weak var lbl: UILabel!
    @IBOutlet weak var txtField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func updateCell(lblText:String?, txtFieldText:String?, isTxtFieldEditable:Bool) {
        lbl.text = lblText ?? ""
        txtField.delegate = self
        if lblText?.caseInsensitiveCompare("Mobile") == .orderedSame {
            txtField.keyboardType = .numberPad
        } else if lblText?.caseInsensitiveCompare("Email") == .orderedSame {
            txtField.keyboardType = .emailAddress
        }
        txtField.text = txtFieldText
        if isTxtFieldEditable {
            txtField.isEnabled = true
        } else {
            txtField.isEnabled = false
        }
    }
}

extension TextFieldCell : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
