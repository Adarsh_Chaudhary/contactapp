//
//  File.swift
//  Contacts
//
//  Created by adarsh chaudhary on 21/07/19.
//  Copyright © 2019 adarsh. All rights reserved.
//

import Foundation
import UIKit

class CommonUtility {
    
    class func errorViewForTableHeader(text : String, frame:CGRect) -> UIView {
        let lbl = UILabel.init(frame: frame)
        lbl.textAlignment = .center
        lbl.text = text
        return lbl
    }
}
