//
//  BaseRequestManager.swift
//  Contacts
//
//  Created by adarsh chaudhary on 20/07/19.
//  Copyright © 2019 adarsh. All rights reserved.
//

import Foundation

class BaseRequestManager : NSObject {
    
    static let sharedManager = BaseRequestManager()
    let header = [
        "Content-Type" : "application/json"
    ]

    func createGetRequest(params:[String:AnyObject]?, url:URLRequest, completion:@escaping (_ response:AnyObject?,_ error: ApiError?) -> ()) {
        var urlRequest = url
        urlRequest.allHTTPHeaderFields = header
        urlRequest.httpMethod = "GET"
        if let _ = params {
            do {
                urlRequest.httpBody = try JSONSerialization.data(withJSONObject: params ?? [:], options: .prettyPrinted)
            } catch let error {
                print(error.localizedDescription)
            }
        }
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, urlResponse, error) in
            if let _ = error {
                let err = ApiError.init(dict: ["error" : "Oops!! Something Went Wrong" as AnyObject])
                completion(nil, err)
            } else {
                let json = try? JSONSerialization.jsonObject(with: data!, options: [])
                if let response = json as? [String:AnyObject] {
                    if response.keys.contains("error") {
                        let err = ApiError.init(dict: response)
                        completion(nil, err)
                    } else {
                        completion(json as AnyObject?, nil)
                    }
                } else {
                    completion(json as AnyObject?, nil)
                }
            }
        }
        task.resume()
    }
    
    func createDeleteRequest(params:[String:AnyObject]?, url:URLRequest, completion:@escaping (_ response:AnyObject?, _ error: ApiError?)->()) {
        var urlRequest = url
        urlRequest.httpMethod = "DELETE"
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, urlResponse, error) in
            if let _ = error {
                let err = ApiError.init(dict: ["error" : "Oops!! Something Went Wrong" as AnyObject])
                completion(nil, err)
            } else {
                let json = try? JSONSerialization.jsonObject(with: data!, options: [])
                if let response = json as? [String:AnyObject] {
                    if response.keys.contains("error") {
                        let err = ApiError.init(dict: response)
                        completion(nil, err)
                    } else {
                        completion(json as AnyObject?, nil)
                    }
                } else {
                    completion(json as AnyObject?, nil)
                }
            }
        }
        task.resume()
    }
    
    func createPostRequest(params:[String:AnyObject]?, url:URLRequest, completion:@escaping (_ response:AnyObject?, _ error:ApiError?)->()) {
        var urlRequest = url
        urlRequest.httpMethod = "POST"
        urlRequest.allHTTPHeaderFields = header
        if let _ = params {
            do {
                urlRequest.httpBody = try JSONSerialization.data(withJSONObject: params ?? [:], options: .prettyPrinted)
            } catch let error {
                print(error.localizedDescription)
            }
        }
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, urlResponse, error) in
            if let _ = error {
                let err = ApiError.init(dict: ["error" : "Oops!! Something Went Wrong" as AnyObject])
                completion(nil, err)
            } else {
                let json = try? JSONSerialization.jsonObject(with: data!, options: [])
                if let response = json as? [String:AnyObject] {
                    if response.keys.contains("error") {
                        let err = ApiError.init(dict: response)
                        completion(nil, err)
                    } else {
                        completion(json as AnyObject?, nil)
                    }
                } else {
                    completion(json as AnyObject?, nil)
                }
            }
        }
        task.resume()
    }
    
    func createPutRequest(params:[String:AnyObject]?, url:URLRequest, completion:@escaping (_ response:AnyObject?, _ error:ApiError?)->()) {
        var urlRequest = url
        urlRequest.httpMethod = "PUT"
        urlRequest.allHTTPHeaderFields = header
        if let _ = params {
            do {
                urlRequest.httpBody = try JSONSerialization.data(withJSONObject: params ?? [:], options: .prettyPrinted)
            } catch let error {
                print(error.localizedDescription)
            }
        }
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, urlResponse, error) in
            if let _ = error {
                let err = ApiError.init(dict: ["error" : "Oops!! Something Went Wrong" as AnyObject])
                completion(nil, err)
            } else {
                let json = try? JSONSerialization.jsonObject(with: data!, options: [])
                if let response = json as? [String:AnyObject] {
                    if response.keys.contains("error") {
                        let err = ApiError.init(dict: response)
                        completion(nil, err)
                    } else {
                        completion(json as AnyObject?, nil)
                    }
                } else {
                    completion(json as AnyObject?, nil)
                }
            }
        }
        task.resume()
    }
}
