//
//  RequestManager.swift
//  Contacts
//
//  Created by adarsh chaudhary on 19/07/19.
//  Copyright © 2019 adarsh. All rights reserved.
//

import Foundation
import UIKit

class RequestManager : NSObject{
    static let sharedManager = RequestManager.init()
    
    func getContactList(params:[String:AnyObject]?, completionHandler:@escaping (_ data:[Contacts]?, _ error : ApiError?)->()) {
        let urlString = "http://gojek-contacts-app.herokuapp.com/contacts.json"
        if let url = URL.init(string: urlString) {
            let urlRequest = URLRequest.init(url: url)
            BaseRequestManager.sharedManager.createGetRequest(params: params, url: urlRequest) { (response, error) in
                if let _ = error {
                    completionHandler(nil, error)
                } else {
                    let contactList = response as? [[String:AnyObject]]
                    var contacts : [Contacts] = []
                    for contact in contactList ?? [] {
                        contacts.append(Contacts.init(json: contact))
                    }
                    completionHandler(contacts, nil)
                }
            }
        }
    }
    
    func getContactDetail(urlString :String, params:[String:AnyObject]?, completionHandler:@escaping(_ data:ContactDetailModel?, _ error : ApiError?)->()) {
        if let url = URL.init(string: urlString) {
            let urlRequest = URLRequest.init(url: url)
            BaseRequestManager.sharedManager.createGetRequest(params: params, url: urlRequest) { (response, error) in
                if let _ = error {
                    completionHandler(nil, error)
                } else {
                    let contactDetail = response as? [String:AnyObject]
                    let contactModel = ContactDetailModel.init(dict: contactDetail ?? [:])
                    completionHandler(contactModel, nil)
                }
            }
        }
    }
    
    func deleteContactDetails(params:[String:AnyObject]?, contactId:NSNumber, completionHandler:@escaping (_ response : AnyObject?,_ error: ApiError?)->()) {
        if let url = URL.init(string: "http://gojek-contacts-app.herokuapp.com/contacts/\(contactId ).json") {
            let urlRequest = URLRequest.init(url: url)
            BaseRequestManager.sharedManager.createDeleteRequest(params: params, url: urlRequest) { (response, error) in
                if let _ = error {
                    completionHandler(nil, error)
                } else {
                    completionHandler(response, nil)
                }
            }
        }
    }
    
    func addContact(params:[String:AnyObject]?, completionHandler:@escaping (_ response: ContactDetailModel?, _ error:ApiError?)->()) {
        if let url = URL.init(string: "http://gojek-contacts-app.herokuapp.com/contacts.json") {
            let urlRequest = URLRequest.init(url: url)
            BaseRequestManager.sharedManager.createPostRequest(params: params, url: urlRequest) { (response, error) in
                if let _ = error {
                    completionHandler(nil, error)
                } else {
                    let responseDict = response as? [String:AnyObject]
                    let contactModel = ContactDetailModel.init(dict: responseDict ?? [:])
                    completionHandler(contactModel, nil)
                }
            }
        }
    }
    
    func editContact(params:[String:AnyObject]?, contactId:NSNumber, completionHandler:@escaping (_ response: ContactDetailModel?, _ error:ApiError?)->()) {
        if let url = URL.init(string: "http://gojek-contacts-app.herokuapp.com/contacts/\(contactId ).json") {
            let urlRequest = URLRequest.init(url: url)
            BaseRequestManager.sharedManager.createPutRequest(params: params, url: urlRequest) { (response, error) in
                if let _ = error {
                    completionHandler(nil, error)
                } else {
                    let responseDict = response as? [String:AnyObject]
                    let contactModel = ContactDetailModel.init(dict: responseDict ?? [:])
                    completionHandler(contactModel, nil)
                }
            }
        }
    }
    
    func getImage(url:String?, completionHandler:@escaping (_ image:UIImage?)->()) {
        if url != nil && url != "" {
            let requestUrl = URL.init(string: url!)!
            let request = URLRequest(url: requestUrl)
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                if let _ = error {
                    completionHandler(UIImage.init(named: "photo_placeholder"))
                } else {
                    if let image = UIImage.init(data: data ?? Data()) {
                        completionHandler(image)
                    } else {
                        completionHandler(UIImage.init(named: "photo_placeholder"))
                    }
                }
            }
            task.resume()
        } else {
            completionHandler(UIImage.init(named: "photo_placeholder"))
        }
    }
}
