//
//  ContactsUITests.swift
//  ContactsUITests
//
//  Created by adarsh chaudhary on 19/07/19.
//  Copyright © 2019 adarsh. All rights reserved.
//

import XCTest

class ContactsUITests: XCTestCase {

    var app : XCUIApplication?
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        app = XCUIApplication()
        app?.launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testAddContact() {
        let existsPredicate = NSPredicate(format: "exists == true")
        let element = app?.tables["contactList"]
        expectation(for: existsPredicate, evaluatedWith: element, handler: nil)
        waitForExpectations(timeout: 8, handler: nil)
        if app?.navigationBars.buttons["addContactNavBtn"].isHittable ?? false {
            app?.navigationBars.buttons["addContactNavBtn"].tap()
            let element = app?.tables["addContactTable"]
            XCTAssertEqual(element?.cells.count, 4)
        } else {
            XCTAssert(false)
        }
    }
}
